dxvk (1.7.2+ds1-4) unstable; urgency=medium

  * Pass the proper wine libdir to the build.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Tue, 01 Dec 2020 23:05:33 +0900

dxvk (1.7.2+ds1-3) unstable; urgency=medium

  * Match architectures with those supported by wine-development.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Tue, 01 Dec 2020 18:46:26 +0900

dxvk (1.7.2+ds1-2) unstable; urgency=medium

  * Pass dpkg-buildflags to Meson.
  * Fix dxvk-setup to work on path containing spaces (thanks to
    Guillaume Charifi).
  * Bump debhelper compat level to 13.
  * Bump Standards-Version to 4.5.0 (no changes needed).
  * Bump watch version to 4.
  * Update patches metadata.
  * Add lintian override for maintainer-manual-page for Debian-specific
    version of dxvk-setup.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Tue, 10 Nov 2020 17:34:50 +0900

dxvk (1.7.2+ds1-1) unstable; urgency=medium

  * New upstream release (Closes: #952966, #954539).
  * Adapt to build using mingw as is done upstream now.
  * Add myself to uploaders (with maintainer's consent).

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Wed, 21 Oct 2020 12:29:37 +0900

dxvk (1.5.4+ds1-2) unstable; urgency=medium

  * Tests: check for d3d9.dll.

 -- Alexandre Viau <aviau@debian.org>  Sun, 09 Feb 2020 20:37:54 -0500

dxvk (1.5.4+ds1-1) unstable; urgency=medium

  * New upstream version.
  * dxvk-setup: install d3d9 dll.

 -- Alexandre Viau <aviau@debian.org>  Sun, 09 Feb 2020 17:19:17 -0500

dxvk (1.4.6+ds1-2) unstable; urgency=medium

  * Only support amd64 and any-i386.

 -- Alexandre Viau <aviau@debian.org>  Thu, 16 Jan 2020 11:20:02 -0500

dxvk (1.4.6+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sun, 15 Dec 2019 18:19:14 -0500

dxvk (1.4.3+ds1-2) unstable; urgency=medium

  * setup-dxvk: Echo errors to stderr.
  * Install 32bit libs in 64bit prefixes. (Closes: #925563)

 -- Alexandre Viau <aviau@debian.org>  Sun, 15 Dec 2019 17:13:19 -0500

dxvk (1.4.3+ds1-1) unstable; urgency=medium

  * New upstream version.
  * Refresh dxvk-version.patch.

 -- Alexandre Viau <aviau@debian.org>  Fri, 25 Oct 2019 17:18:57 -0400

dxvk (0.96+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 26 Jan 2019 23:21:44 -0500

dxvk (0.95+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 12 Jan 2019 19:28:29 -0500

dxvk (0.94+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 15 Dec 2018 13:02:25 -0500

dxvk (0.93+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 24 Nov 2018 12:42:08 -0500

dxvk (0.92+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sun, 11 Nov 2018 13:29:17 -0500

dxvk (0.91+ds1-14) unstable; urgency=medium

  * autopkgtest: allow-stderr.

 -- Alexandre Viau <aviau@debian.org>  Wed, 07 Nov 2018 23:18:41 -0500

dxvk (0.91+ds1-13) unstable; urgency=medium

  * dxvk-setup: support scenario where wine64 is installed without
    wine32.
  * dxvk-setup: exit 1 when no action was completed.

 -- Alexandre Viau <aviau@debian.org>  Wed, 07 Nov 2018 10:21:30 -0500

dxvk (0.91+ds1-12) unstable; urgency=medium

  * augopkgtest: depend on wine-development.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Nov 2018 22:02:28 -0500

dxvk (0.91+ds1-11) unstable; urgency=medium

  * reformat test script.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Nov 2018 14:46:45 -0500

dxvk (0.91+ds1-10) unstable; urgency=medium

  * autopkgtest: don't require multi-arch.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Nov 2018 14:28:59 -0500

dxvk (0.91+ds1-9) unstable; urgency=medium

  * autopkgtest: explicitly list all dxvk dependencies.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Nov 2018 12:29:04 -0500

dxvk (0.91+ds1-8) unstable; urgency=medium

  * autopkgtest: ensure that tests use wine-development.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Nov 2018 09:31:23 -0500

dxvk (0.91+ds1-7) unstable; urgency=medium

  * autopkgtest: test custom wineprefix.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Nov 2018 20:27:29 -0500

dxvk (0.91+ds1-6) unstable; urgency=medium

  * autopkgtest: also test uninstall.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Nov 2018 20:24:55 -0500

dxvk (0.91+ds1-5) unstable; urgency=medium

  * autopkgtest: check for symlinks.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Nov 2018 20:10:08 -0500

dxvk (0.91+ds1-4) unstable; urgency=medium

  * Add autopkgtest.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Nov 2018 19:56:23 -0500

dxvk (0.91+ds1-3) unstable; urgency=medium

  * build on more 32bit architectures.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Nov 2018 18:45:07 -0500

dxvk (0.91+ds1-2) unstable; urgency=medium

  * dxvk: set Multi-Arch: foreign.

 -- Alexandre Viau <aviau@debian.org>  Sun, 04 Nov 2018 15:28:29 -0500

dxvk (0.91+ds1-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sun, 04 Nov 2018 15:18:34 -0500

dxvk (0.90+ds3-5) unstable; urgency=medium

  * dxvk-setup: default to the installed wine version.

 -- Alexandre Viau <aviau@debian.org>  Fri, 02 Nov 2018 19:50:33 -0400

dxvk (0.90+ds3-4) unstable; urgency=medium

  * d/control: specify build dependencies per architecture.

 -- Alexandre Viau <aviau@debian.org>  Thu, 01 Nov 2018 17:44:03 -0400

dxvk (0.90+ds3-3) unstable; urgency=medium

  * dxvk-setup: improve wine32 support.
  * dxvk-setup: remove unused quiet variable.
  * d/control: bump standards-version.

 -- Alexandre Viau <aviau@debian.org>  Sun, 21 Oct 2018 13:53:02 -0400

dxvk (0.90+ds3-2) unstable; urgency=medium

  * d/control: recommend dxvk in dxvk-wine* packages.
  * Ship dxvk-setup script.
  * d/control: mention dxvk-setup in dxvk's description.

 -- Alexandre Viau <aviau@debian.org>  Sun, 21 Oct 2018 12:24:09 -0400

dxvk (0.90+ds3-1) unstable; urgency=medium

  * Initial release. (Closes: #911195)

 -- Alexandre Viau <aviau@debian.org>  Thu, 18 Oct 2018 18:11:27 -0400
